﻿using System;
using System.Collections.Generic;
using System.Linq;
using testBlog.Models;
using System.Web.Mvc;

namespace testBlog.Controllers
{
    public class HomeController : Controller
    {
        readonly DatabaseContext context = new DatabaseContext();
        readonly string fail = "Sorry, things gone wrong!";
        readonly string head = "<div align=\"center\">" +
            "<a href=\"/Home/Index\"><h2>Superblog!</h2></a>" +
            "</div>";
        readonly byte PageSize = 5;

        public ActionResult Index()
        {
            ViewBag.page = 1;
            ViewBag.result = fail;
            try
            {
                IEnumerable<Publication> pubSet = context
                                                .Publications
                                                .OrderByDescending(p => p.PublicationId)
                                                .Take(PageSize);
                ViewBag.publ = pubSet;
                ViewBag.succes = true;
            }
            catch (Exception ex)
            {
                ViewBag.succes = false;
                ViewBag.result = fail + "<br/>" + ex.ToString();
            }
            return View();
        }
        [HttpGet]
        public ActionResult Index(int id = 1)
        {
            if (id < 1)
            {
                id = 1;
            }
            var count = context.Publications.Count();
            var maxPagesCount = count / PageSize + 1;
            if (count < (id - 1) * PageSize)
            {
                id = maxPagesCount;
            }

            ViewBag.page = id;
            ViewBag.pageMax = maxPagesCount;
            ViewBag.result = fail;
            try
            {
                IEnumerable<Publication> pubSet = context
                                                .Publications
                                                .OrderByDescending(p => p.PublicationId);
                var pubset = pubSet.Skip((id - 1) * PageSize)
                                   .Take(PageSize);
                ViewBag.publ = pubset;
                ViewBag.succes = true;
            }
            catch (Exception ex)
            {
                ViewBag.succes = false;
                ViewBag.result = fail + "<br/>" + ex.ToString();
            }
            return View();
        }


        [HttpGet]
        public ActionResult Read(int id = -1)
        {
            ViewBag.result = string.Empty;
            try
            {
                var publication = context.Publications
                                .Where(p => p.PublicationId == id)
                                .First();
                ViewBag.publication = publication;
                ViewBag.succes = true;
            }
            catch (Exception ex)
            {
                ViewBag.result = fail + "<br/>" + ex.ToString();
                ViewBag.succes = false;
            }
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public string Create(Publication publication)
        {
            string ret = string.Empty;
            if (publication != null)
            {
                try
                {
                    publication.PublicationTime = DateTime.Now;
                    context.Publications.Add(publication);
                    context.SaveChanges();
                    ret = "Ok, " + publication.Author + " its 'lredy posted.";
                }
                catch (Exception ex)
                {
                    ret = fail + ex.ToString();
                }
            }
            else
            {
                ret = fail;
            }
            return head + ret;
        }

        [HttpGet]
        public ActionResult Edit(int id = -1)
        {
            ViewBag.result = fail;
            try
            {
                var publication = context.Publications
                                .Where(p => p.PublicationId == id)
                                .First();
                ViewBag.publication = publication;
                ViewBag.succes = true;
            }
            catch (Exception ex)
            {
                ViewBag.result = fail + "<br/>" + ex.ToString();
                ViewBag.succes = false;
            }
            return View();
        }
        [HttpPost]
        public string Edit(Publication publication)
        {
            string ret = string.Empty;
            if (publication != null)
            {
                try
                {
                    publication.PublicationTime = DateTime.Now;
                    var basePubl = context.Publications
                                    .Where(p => p.PublicationId == publication.PublicationId)
                                    .First();

                    basePubl.Author = publication.Author;
                    basePubl.Title = publication.Title;
                    basePubl.FullText = publication.FullText;
                    basePubl.Description = publication.Description;

                    context.SaveChanges();
                    ret = "Ok, " + publication.Author + " its 'lredy edited.";
                }
                catch (Exception ex)
                {
                    ret = fail + "<br/>" + ex.ToString();
                }
            }
            else
            {
                ret = fail;
            }
            return head + "<br/>" + ret;
        }

        [HttpGet]
        public string Delete(int id = -1)
        {
            string ret = string.Empty;
            try
            {
                var publication = context.Publications
                                .Where(p => p.PublicationId == id)
                                .First();
                context.Publications.Remove(publication);
                context.SaveChanges();
                ret = "Succesfully deleted!";
            }
            catch
            {
                ret = fail;
            }
            return head + "<br/>" + ret;
        }
        //[HttpGet]
        //public ActionResult Delete(int id)
        //{
        //    ViewBag.idToDelete = id;
        //    return View();
        //}
        //[HttpPost]
        //public string Delete(bool confirm, int id)
        //{
        //    string ret = string.Empty;
        //    if (confirm == true)
        //    {
        //        try
        //        {
        //            var publication = context.Publications
        //                            .Where(p => p.PublicationId == id)
        //                            .First();
        //            context.Publications.Remove(publication);
        //            ret = "Publication " + publication.Title + "have been deleted.";
        //        }
        //        catch (Exception ex)
        //        {
        //            ret = fail + "<br/>" + ex.ToString();
        //        }
        //    }
        //    else
        //    {
        //        ret = "Publication was not deleted!";
        //    }
        //    return head + "<br/>" + ret;
        //}
    }
}