﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace testBlog.Models
{
    public class Publication
    {
        public int PublicationId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FullText { get; set; }
        public string Author { get; set; }
        public DateTime PublicationTime { get; set; }
    }
    public class DatabaseContext : DbContext
    {
        public DbSet<Publication> Publications { get; set; }
    }
}