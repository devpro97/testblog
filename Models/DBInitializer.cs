﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace testBlog.Models
{
    public class DBInitializer : DropCreateDatabaseAlways<DatabaseContext>
    {
        protected override void Seed(DatabaseContext db)
        {
            db.Publications.Add(new Publication
            {
                Title = "title1",
                Description = "descr1",
                Author = "author 1",
                PublicationTime = DateTime.Now,
                FullText = "Domitio Ap. Claudio consulibus, discedens ab hibernis Caesar in Italiam, " +
                "ut quotannis facere consuerat, legatis imperat quos legionibus praefecerat uti quam " +
                "plurimas possent hieme naves aedificandas veteresque reficiendas curarent."
            });
            db.Publications.Add(new Publication
            {
                Title = "title2",
                Description = "descr2",
                Author = "author 2",
                PublicationTime = DateTime.Now,
                FullText = "Earum modum formamque demonstrat. Ad celeritatem onerandi subductionesque " +
                "paulo facit humiliores quam quibus in nostro mari uti consuevimus, atque id eo magis," +
                " quod propter crebras commutationes aestuum minus magnos ibi fluctus fieri cognoverat" +
                "; ad onera, ad multitudinem iumentorum transportandam paulo latiores quam quibus in r" +
                "eliquis utimur maribus."
            });
            db.Publications.Add(new Publication
            {
                Title = "title3",
                Description = "descr3",
                Author = "author 3",
                PublicationTime = DateTime.Now,
                FullText = "Has omnes actuarias imperat fieri, quam ad rem multum humilitas adiuvat. E" +
                "a quae sunt usui ad armandas naves ex Hispania apportari iubet. Ipse conventibui Gall" +
                "iae citeribris peractis in Illyricum proficiscitur, quod a Pirustis finitimam partem " +
                "provinciae incursionibus vastari audiebat."
            });
            db.Publications.Add(new Publication
            {
                Title = "title4",
                Description = "descr4",
                Author = "author 4",
                PublicationTime = DateTime.Now,
                FullText = "Eo cum venisset, civitatibus milites imperat certumque in locum convenire i" +
                "ubet. Qua re nuntiata Pirustae legatos ad eum mittunt qui doceant nihil earum rerum pu" +
                "blico factum consilio, seseque paratos esse demonstrant omnibus rationibus de iniuriis" +
                " satisfacere."
            });
            db.Publications.Add(new Publication
            {
                Title = "title5",
                Description = "descr5",
                Author = "author 5",
                PublicationTime = DateTime.Now,
                FullText = "Accepta oratione eorum Caesar obsides imperat eosque ad certam diem adduci i" +
                "ubet; nisi ita fecerint, sese bello civitatem persecuturum demonstrat. Eis ad diem addu" +
                "ctis, ut imperaverat, arbitros inter civitates dat qui litem aestiment poenamque consti" +
                "tuant."
            });
            db.Publications.Add(new Publication
            {
                Title = "title6",
                Description = "descr6",
                Author = "author 6",
                PublicationTime = DateTime.Now,
                FullText = "His confectis rebus conventibusque peractis, in citeriorem Galliam revertitu" +
                "r atque inde ad exercitum proficiscitur. Eo cum venisset, circuitis omnibus hibernis, s" +
                "ingulari militum studio in sumrma omnium rerum inopia circiter sescentas eius generis c" +
                "uius supra demonstravimus naves et longas XXVIII invenit instructas neque multum abesse" +
                " ab eo quin paucis diebus deduci possint."
            });

            base.Seed(db);
        }
    }
}